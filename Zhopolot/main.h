#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent/SRDescent.h>
#include <SRHook.hpp>

class AsiPlugin : public SRDescent {
	SRHook::Hook<> updatePads{ 0x541E1C, 5 };

	bool invertUD = false;
	bool invertLR = false;

public:
	explicit AsiPlugin();

protected:
	void processInvert();

private:
	void updateInvert( bool &invert );
};

#endif // MAIN_H
