#include "main.h"
#include <gtasa.hpp>

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	updatePads.onBefore += std::tuple{ this, &AsiPlugin::processInvert };
	updatePads.install( 0, 0, false );
}

void AsiPlugin::processInvert() {
	if ( !LOCAL_PLAYER || !LOCAL_PLAYER->isDriver() || !LOCAL_PLAYER->vehicle || !LOCAL_PLAYER->vehicle->isPlane() ) return;

	auto &ctrl = CPad::get()->NewState;

	if ( !ctrl.LeftStickY ) updateInvert( invertUD );
	if ( !ctrl.LeftShoulder2 && !ctrl.RightShoulder2 ) updateInvert( invertLR );

	if ( invertUD ) ctrl.LeftStickY *= -1;
	if ( invertLR ) std::swap( ctrl.LeftShoulder2, ctrl.RightShoulder2 );
}

void AsiPlugin::updateInvert( bool &invert ) {
	auto coup = LOCAL_PLAYER->vehicle->matrix->at.fZ;
	if ( coup < -0.5f )
		invert = true;
	else if ( coup > 0.5f )
		invert = false;
}
